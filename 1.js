// https://www.codewars.com/kata/5777fe3f355edbf0a5000d11

const xMarksTheSpot = input => {
  if (!input.length) return [];

  const coordinates = [];

  for (let xIndex = 0; xIndex < input.length; xIndex++) {
    if (!input[xIndex].length) return [];

    for (let yIndex = 0; yIndex < input[xIndex].length; yIndex++) {
      if (input[xIndex][yIndex] === 'x') {
        if (coordinates.length) {
          return [];
        }

        coordinates.push(xIndex);
        coordinates.push(yIndex);
      }
    }
  }

  return coordinates;
};
